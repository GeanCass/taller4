//Valor numérico de octal a decimal
function octalADecimal()
{
    var numero = parseInt(prompt("Ingrese su número octal"), 8);
    if(isNaN(numero))
    {
        alert("Ingrese un número octal correcto");
    }
    else
    {
        alert("El número en decimal es: " + numero);
    }
}
//Operaciones con funciones
function operaciones()
{
    var numero_1 = parseFloat(prompt("Ingrese su primer operando"));
    var numero_2 = parseFloat(prompt("Ingrese su segundo operando"));
    if(!isNaN(numero_1) && !isNaN(numero_2))
    {
        imprimir("Suma: " + (suma(numero_1,numero_2)));
        imprimir("Resta: " + (resta(numero_1,numero_2)));
        imprimir("Multiplicación: " + (multiplicacion(numero_1,numero_2)));
        imprimir("División: " + (division(numero_1,numero_2)));
    }
    else
    {
        alert("Ambos operadores deben ser números");
    }
}

function suma(operador1, operador2)
{
    return operador1 + operador2;
}

function resta(operador1, operador2)
{
    return operador1 - operador2;
}

function multiplicacion(operador1, operador2)
{
    return operador1 * operador2;
}

function division(operador1, operador2)
{
    return operador1 / operador2;
}

function imprimir(mensaje)
{
    alert(mensaje);
}
